console.log("get rect.js")
var sketchPoints = new SketchPoints();
var rectangles = new Rectangles();
var circles = new Circles();

var circles_rem = [];
var hit = -1;
var r = 0;

function Point(x, y){
    this.x=x;
    this.y=y;
}

function SketchPoints(){
	this.points=[];
	this.minX=-1;
	this.maxX=408;
	this.minY=0;
	this.maxY=209;
}

function Rectangles(){
	this.rectangleList=[];
	this.minX=-1;
	this.maxX=408;
	this.minY=0;
	this.maxY=209;
}

function singleRec( u, i , o, p){
	this.startX = u;
	this.startY = i;
	this.width = o;
	this.hight = p;
}

function Circles(){
	this.circleList = [];
	this.minX= -1;
	this.maxX = 408;
	this.minY = 0;
	this.maxY = 209;
}

function oneCir(u, i, o){
	this.center_x = u;
	this.center_y = i
	this.radius = o;
	
}

function drawAll(){
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	for(var z =0; z< circles_rem.length; z++){
		//console.log("drawall")
		var myCircle = circles_rem[z];
		ctx.beginPath();
		ctx.globalAlpha = 1;
		ctx.arc(myCircle[0], myCircle[1], myCircle[2], 0, 2*Math.PI,false);
		ctx.fillStyle = "#FFD34E";
		ctx.fill();
	}
	
}



function getRandomInt(min, max) {
	  return Math.floor(Math.random() * (max - min + 1)) + min;
	}

var isDrawing, myPoints = [ ], radius = 15, drawCircle = false;
var circle_x, circle_y, r, singleC, cancel = false, last_r=0, max_w= 0, max_h = 0;

var canvas = document.getElementById("tools_sketch"),
	ctx = canvas.getContext('2d'),
	rect = {},
	drawline = true,
	lastX, lastY,
	drag = false;

function init(){
	console.log("init");
	canvas.addEventListener("mousedown", mouseDown, false);
	canvas.addEventListener("mouseup", mouseUp, false);
	canvas.addEventListener("mousemove", mouseMove, false);
	$('#tools_sketch').mouseleave(function(e){
		drag = false;
		isDrawing = false;
	});
}

function mouseDown(e){
	if(drawCircle){
		 isDrawing = true;
		  myPoints.push({ 
		    x:  e.pageX - this.offsetLeft, 
		    y: e.pageY - this.offsetTop,
		    radius: getRandomInt(5, 20),
		    opacity: Math.random()*0.010
		  });
	}
	else if(singleC){
		drag = true;
		circle_x = e.pageX - this.offsetLeft;
		circle_y = e.pageY - this.offsetTop;
		hit = -1;
		
		for(var z=0; z<circles_rem.length; z++){
			var myCircle = circles_rem[z];
			var dx = circle_x - myCircle[0];
			
			var dy = circle_y - myCircle[1];
			console.log(dx)
			console.log(dy)
			console.log(dx*dx +dy*dy);
			console.log(myCircle[2] * myCircle[2]);
			if(dx*dx +dy*dy < myCircle[2] * myCircle[2]){
				hit = z;
			}
		}
		console.log(hit)
		
	}
	else{
		rect.startX = e.pageX - this.offsetLeft;
		//console.log(rect.startX);
		rect.startY = e.pageY - this.offsetTop;
		//console.log(rect.startY);
		Draw(e.pageX- this.offsetLeft, e.pageY- this.offsetTop, false);
		drag = true;
	}
	
}



function mouseMove(e) {
	
	if(drawCircle){
		 if (isDrawing){
		  
			  myPoints.push({ 
			    x: e.pageX - this.offsetLeft, 
			    y: e.pageY - this.offsetTop,
			    radius: getRandomInt(5, 20),
			    opacity: Math.random()*0.010
			  });
			  for (var i = 0; i < myPoints.length; i++) {
				    ctx.beginPath();
				    ctx.globalAlpha = myPoints[i].opacity;
				    ctx.lineJoin = ctx.lineCap = 'round';
					 ctx.fillStyle = '#EB7F00';
				    ctx.arc(
				      myPoints[i].x, myPoints[i].y, myPoints[i].radius, 
				      false, Math.PI * 2, false);
				    ctx.fill();
				    //console.log("circle")
			}
		 }
		  
	}
	
	else if(singleC && drag){
		
			var center = new Point(circle_x, circle_y);
			
			var temp = new Point(e.pageX - this.offsetLeft,e.pageY - this.offsetTop);
			r = lineDistance(center, temp);
			//console.log("r");
			//if(r< last_r){
			//cir_Draw_white();
			
			//}
			//else 
		if(hit< 0){
			drawAll();
			cir_Draw();
		}
		else{
			circles_rem[hit][0] = e.pageX - this.offsetLeft;
			circles_rem[hit][1] = e.pageY - this.offsetTop;
			
			drawAll();
		}
			//last_r = r;
		
	}
	else{
		if (drag && !drawline) {
			//console.log(Math.abs(-3));
			rect.w = (e.pageX - this.offsetLeft) - rect.startX;
			rect.h = (e.pageY - this.offsetTop) - rect.startY ;
			if(Math.abs(rect.w) > Math.abs(max_w))  max_w = rect.w;
			//console.log(max_w)
			if(Math.abs(rect.h) > Math.abs(max_h))  max_h = rect.h;
		    //ctx.clearRect(0,0,canvas.width,canvas.height);
			draw();
			
		}
		if(drag && drawline){
		Draw(e.pageX-this.offsetLeft, e.pageY-this.offsetTop, true);
		//console.log(drag);
		//console.log(e.pageX);
		}
	}

}



function mouseUp(e) {
	//console.log("mouseup")
	last_r = 0;
	
	if(drawCircle){
		isDrawing = false;
		myPoints.length = 0;
		}
	else{
		drag = false;
		if(!singleC && !drawline){
		console.log(rect.startX);
		console.log((rectangles.maxY-rect.startY));
		console.log(rect.w);
		console.log(rect.h);
		var rectangle = new singleRec(rect.startX,(rectangles.maxY-rect.startY), rect.w, rect.h );
		rectangles.rectangleList.push(rectangle);
		}
		else if(singleC){
		//console.log(circle_x);
		//console.log(circles.maxY-circle_y);
		//console.log(r);
		var circle = new oneCir(circle_x, (circles.maxY-circle_y), r);
		circles.circleList.push(circle);	
		
		if(hit < 0){
			circles_rem.push([
			circle_x,
			circle_y,
			r
			]);
		}
		//console.log(circles_rem[1][1])
		
		}
	}
	
}





function cir_Draw(){
	//console.log("draw cir");
	ctx.beginPath();
	ctx.globalAlpha = 1;
	ctx.arc(circle_x, circle_y, r, 0, 2*Math.PI,false);
	ctx.fillStyle = "#FFD34E";
	ctx.fill();
}

/*
function cir_Draw_white(){
	//console.log("draw white cir");
	ctx.beginPath();
	ctx.globalAlpha = 1;
	ctx.arc(circle_x, circle_y, last_r, 0, 2*Math.PI,false);
	
	ctx.lineWidth = 9;
	ctx.strokeStyle = "#ffffff";
	ctx.stroke();	
}
*/

function draw() {
	
		ctx.globalAlpha = 1;
		ctx.fillStyle= "#ACF0F2";
		ctx.fillRect(rect.startX, rect.startY, rect.w, rect.h);	
	//ctx.stroke();
}


function Draw(x, y, isDown){
	//console.log("Draw");

		if(isDown){
			
			ctx.beginPath();
			ctx.globalAlpha = 1;
			ctx.strokeStyle = "#1695A3";
			ctx.lineWidth = 2.5;
			ctx.lineCap = "round";
			ctx.lineJoin = "round";
			ctx.moveTo(lastX, lastY);
			ctx.lineTo(x,y);
			//console.log(x)
			//console.log(y)
			sketchPoints.points.push(new Point(x, sketchPoints.maxY-y));
			//ctx.closePath();
			ctx.stroke();
			//console.log("path");
		}
		lastX = x; lastY = y;	
	
}

document.getElementById("rect").onclick = function(){
	//console.log("click rect");
	   drawline = false;
	   drawCircle = false;
	   singleC = false;
	   ctx.clearRect(0, 0, canvas.width, canvas.height);
	   document.getElementById("svgLayer").style.display = "none";
		document.getElementById("tools_sketch").style.display = "block";
		clickmodify = false;
}

init();

document.getElementById("clear").onclick = function(){
		sketchPoints = new SketchPoints();
	 	ctx.clearRect(0, 0, canvas.width, canvas.height);
	 	document.getElementById("svgLayer").style.display = "none";
		document.getElementById("tools_sketch").style.display = "block";
		clickmodify = false;
		circles_rem = [];
		hit = -1;
	
}


document.getElementById("line").onclick = function(){
	   drawline = true;
	   drawCircle = false;
	   singleC = false;
	  // console.log("stop drawing circle")
	   ctx.clearRect(0, 0, canvas.width, canvas.height);
	   document.getElementById("svgLayer").style.display = "none";
	   document.getElementById("tools_sketch").style.display = "block";
	   //disableDragAndDrop();
	   clickmodify = false;
	  
}

document.getElementById("circle").onclick = function(){
	   drawCircle = true;
	   drawline =false;
	   singleC = false;
	   ctx.clearRect(0, 0, canvas.width, canvas.height);
	   document.getElementById("svgLayer").style.display = "none";
	   document.getElementById("tools_sketch").style.display = "block";
	   //disableDragAndDrop();
	   clickmodify = false;
	  
}

document.getElementById("singlecircle").onclick = function(){
	   drawCircle = false;
	   drawline =false;
	   singleC = true;
	  //console.log(singleC);
	   ctx.clearRect(0, 0, canvas.width, canvas.height);
	   document.getElementById("svgLayer").style.display = "none";
	   document.getElementById("tools_sketch").style.display = "block";
	   //disableDragAndDrop();
	   clickmodify = false;
}

document.getElementById("eraseCir").onclick = function(){
		if(hit > -1){
	 	circles_rem.splice(hit, 1);
		}
		drawAll();
	   document.getElementById("svgLayer").style.display = "none";
	   document.getElementById("tools_sketch").style.display = "block";
	   //disableDragAndDrop();
	   clickmodify = false;
	  
}

/*
document.getElementById("cancel").onclick = function(){
	if(!drawline && !drawCircle &&! singleC)
	   cancel = true;
	//console.log("canceling")
	 // console.log(max_w)
			ctx.clearRect(rect.startX, rect.startY, max_w, max_h);
			max_w = 0;
			max_h = 0;
			var tem = rectangles.rectangleList.pop();
			console.log(tem);
		
}

*/

function trendAnalysis(){
	console.log("processing")
	var myQuery = getSimilarTrendData();
	console.log(myQuery)
	if(clickmodify){
		sketchPoints = new SketchPoints();
		for(var i =0; i<list.length; i++){
			tempPoint = new Point(list[i][0],list[i][1]);
			//tempPoint = newPoint(list[i][0], sketchPoints.maxY-list[i][1]);
			sketchPoints.points.push(tempPoint);
		}
	}
	myQuery.sketchPoints=sketchPoints;
	//myQuery.rectangles=rectangles; 
	//myQuery.circles=circles;
	getData(myQuery);
	
	/*
	 console.log("hello");
    //this.stopPainting();
    var Data = [];
    var col1 = 0; var c1 = 0;
    var col2 = 0; var c2 = 0;
    var col3 = 0; var c3 = 0;
    var col4 = 0; var c4 = 0;
    //var tempxmin=0;var tempymin=0; var tempxmax=0;var tempymax=0;
    // var a = Math.round((listOfPoints[2].x)/136.3+1);
    //var b = Math.round((4-(listOfPoints[2].y)/102.25)*100)/100;
    //console.log(listOfPoints[2].x);
    //console.log(listOfPoints[2].y);
    for(var i=0; i<listOfPoints.length; i++){
      //if(listOfPoints[i].x<tempxmin) tempxmin= listOfPoints[i].x; if(listOfPoints[i].y<tempymin) tempymin= listOfPoints[i].y;if(listOfPoints[i].x>tempxmax) tempxmax= listOfPoints[i].x;if(listOfPoints[i].y>tempymax) tempymax= listOfPoints[i].y;
   
        var X = Math.round((listOfPoints[i].x)/102.25);
        var Y = Math.round((4-(listOfPoints[i].y-4)/51)*100)/100;
        //console.log(Y);
      if(X == 1){ col1= col1+Y; c1++;}
      if(X == 2){ col2 = col2+Y; c2++;}
      if(X == 3) {col3 = col3+Y; c3++;}
      if(X == 4) {col4 = col4+Y; c4++;}
    
    }
   // console.log(c2,Math.round((col2/c2)*100)/100);
    //console.log(tempxmin,tempymin,tempxmax,tempxmax);
    if(c1!=0) Data.push(new Point(1, Math.round((col1/c1)*100)/100));
    else Data.push(new Point(-1,-1));
    if(c2!=0) Data.push(new Point(2, Math.round((col2/c2)*100)/100));
    else Data.push(new Point(-1,-1));
    if(c3!=0) Data.push(new Point(3, Math.round((col3/c3)*100)/100));
    else Data.push(new Point(3,-1));
    if(c4!=0) Data.push(new Point(4, Math.round((col4/c4)*100)/100));
    else Data.push(new Point(4,-1));
    //console.log(Data[0], Data[1], Data[2], Data[3]);
     //console.log(Data);
     //console.log(Data[1]);

     var jsonData = [];
     var DataX =[];
     var DataY =[];
     //tempx = 0;  tempy = 0;
     var imported = document.createElement('script');
     imported.src = 'comboboxes.js';
     document.head.appendChild(imported);
    */
}




	
	
	 

	  
	  //ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
	  
function lineDistance( point1, point2 )
{
  var xs = 0;
  var ys = 0;

  xs = point2.x - point1.x;
  xs = xs * xs;

  ys = point2.y - point1.y;
  ys = ys * ys;

  return Math.sqrt( xs + ys );
}
	
	




