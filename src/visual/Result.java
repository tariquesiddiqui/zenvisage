package visual;

import java.util.ArrayList;

public class Result {
	public ArrayList<Chart> outputCharts = new ArrayList<Chart>();
	public String method;
	public String xUnit;
	public String yUnit;
	
	// default constructor
	public Result() {
		
	}

	public ArrayList<Chart> getOutputCharts() {
		return outputCharts;
	}

	public void setOutputCharts(ArrayList<Chart> outputCharts) {
		this.outputCharts = outputCharts;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getxUnit() {
		return xUnit;
	}

	public void setxUnit(String xUnit) {
		this.xUnit = xUnit;
	}

	public String getyUnit() {
		return yUnit;
	}

	public void setyUnit(String yUnit) {
		this.yUnit = yUnit;
	}
}
