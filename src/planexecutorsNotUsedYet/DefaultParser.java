package planexecutorsNotUsedYet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import api.Query;
import api.Query.CompositeFilter;
import api.Query.CompositeFilterOperator;
import api.Query.Filter;
import api.Query.FilterPredicate;

public  class DefaultParser {
	
	public static PlanNode createPlan(Query query){
		PlanNode planNode=createScanPlanNode(query);
		planNode=createGroupByPlanNode(query,planNode);
		planNode=createAggregationPlanNode(query, planNode);
		planNode=createProjectionPlanNode(query, planNode);
		planNode=createResultPlanNode(query,planNode);
		return planNode;
	}


	private static PlanNode createScanPlanNode(Query query) {
		// TODO Auto-generated method stub
		Query.Filter filter= query.getFilter();
		if(filter==null){
			List<String> fields=query.getProjections();
			String[] groupBy= query.getGroupBy().split(",");
			fields.addAll(Arrays.asList(groupBy));
			fields.add(query.getAggregationVarible());
		}

		
		if(filter.isComposite())
			return createCompositeFilterPlanNode(query.getFilter());
		else 
			return createPredicateFilterPlanNode((FilterPredicate) query.getFilter());
		
	}

	private static PlanNode createPredicateFilterPlanNode(FilterPredicate filter) {
		// TODO Auto-generated method stub
	   	
		
	   return null;
		
	}
	
	private static PlanNode createIndexedPredicateFilterPlanNode(Query.FilterPredicate indexedPredicate) {
		 PlanNode planNode= new BitmapScanPlanNode(indexedPredicate);
		 return planNode;
		
	}
	
	private static PlanNode createCompositeIndexedFilterPlanNode(List<Query.FilterPredicate> indexedPredicates,
			CompositeFilterOperator compositeFilterOperator
			 ) {
		
		PlanNode planNode=createIndexedPredicateFilterPlanNode(indexedPredicates.get(0));
		for(int i=1;i<indexedPredicates.size();i++){
			if(compositeFilterOperator==CompositeFilterOperator.AND){
				planNode= new BitmapAndPlanNode(indexedPredicates.get(i), planNode);
				
			}
			
			if(compositeFilterOperator==CompositeFilterOperator.OR){
				planNode= new BitmapAndPlanNode(indexedPredicates.get(i), planNode);
				
			}
		}
	
		return planNode;
	}
	
  	

	private static PlanNode createCompositeFilterPlanNode(Filter filter) {
		// TODO Auto-generated method stub
		CompositeFilter compositeFilter = (CompositeFilter) filter;
		List<Query.FilterPredicate> unIndexedPredicates = new ArrayList<Query.FilterPredicate>();
		List<Query.FilterPredicate> indexedPredicates = new ArrayList<Query.FilterPredicate>();
		List<Filter> compositeFilters = new ArrayList<Filter>();
		segregateIndexedUnIndexedandCompositePredicates(filter,indexedPredicates,unIndexedPredicates,compositeFilters);
		PlanNode planNode = createCompositeIndexedFilterPlanNode(indexedPredicates,compositeFilter.getOperator());
		
		
		return null;
	}

	private static void segregateIndexedUnIndexedandCompositePredicates(
			Filter filter, List<FilterPredicate> indexedPredicates,
			List<FilterPredicate> unIndexedPredicates,
			List<Filter> compositeFilters) {
		// TODO Auto-generated method stub
		
	}


	static PlanNode createGroupByPlanNode(Query query, PlanNode planNode){
		return null;
	}


	static PlanNode createAggregationPlanNode(Query query, PlanNode planNode){
		return null;
	}


	static PlanNode createProjectionPlanNode(Query query, PlanNode planNode){
		return null;
	}

	static PlanNode createResultPlanNode(Query query, PlanNode planNode){
		return null;
	}

}
