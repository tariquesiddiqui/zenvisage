/**
 * 
 */
package cluster;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.DoublePoint;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;

import normalization.Normalization;
import api.Args;
import distance.Distance;

/**
 * @author xiaofo
 *
 */
public class KMeans extends Clustering {

	/**
	 * @param distance
	 * @param normalization
	 * @param args
	 */
	public KMeans(Distance distance,
			Normalization normalization, Args args) {
		super(distance, normalization,
				args);
		// TODO Auto-generated constructor stub
	}
	
	/* (non-Javadoc)
	 * @see cluster.Clustering#calculateClusters(double, int, double[][])
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public List calculateClusters(double eps, int k, double[][] normalizedgroups) {
		// TODO Auto-generated method stub
		KMeansPlusPlusClusterer<DoublePoint> kmeans = new KMeansPlusPlusClusterer<DoublePoint>(this.args.getOutlierCount(), 15);
		List<DoublePoint> dataset = new ArrayList<DoublePoint>();
		for(int i = 0;i < normalizedgroups.length; i++) {
			dataset.add(new DoublePoint(normalizedgroups[i]));
		}		
		List<CentroidCluster<DoublePoint>> clusters = kmeans.cluster(dataset);
		return clusters;
	}

	/* (non-Javadoc)
	 * @see cluster.Clustering#computeRepresentativeTrends(java.util.List)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public double[][] computeRepresentativeTrends(List clusters) {
		// TODO Auto-generated method stub
		double[][] representativeTrends = new double[clusters.size()][];
	    for(int k = 0; k < clusters.size(); k++){	    	  
	    	DoublePoint point = (DoublePoint) ((CentroidCluster<DoublePoint>) clusters.get(k)).getCenter();
	  	  	double[] p = point.getPoint();
	  	  	representativeTrends[k] = p; 
	    }
	    return representativeTrends;
	}
}
