package org.vde.database;

import java.util.HashMap;
import java.util.Map;

import com.googlecode.javaewah.datastructure.BitSet;

public class Node {
	public boolean isleaf=false;
	public String type;
	public float value;
	public Map<String,Node> chilNodes=new HashMap<String,Node>();
	public double aggregate;
	
}
