package org.vde.database;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.vde.database.Column;
import org.vde.database.DatabaseMetaData;

import api.Query.FilterPredicate;
import  org.vde.database.ColumnMetadata;

public class Database {
	private String name;
	private Map<String,Column> columns= new HashMap<String,Column>();
	public DatabaseMetaData databaseMetaData= new DatabaseMetaData();
	public static long rowCount;
	
	public Database(String name,String schemafilename,String datafilename) throws IOException, InterruptedException{
		this.name=name;
		readSchema(schemafilename);
		loadData(datafilename);
		DatabaseCatalog.addDatabase(name, this);		
	}
	
	public Map<String, Column> getColumns() {
		return columns;
	}

	private void addValue(String columnName,int row,String value){
		Column column=columns.get(columnName);
		column.add(row, value);
 	}
	
	
	private void readSchema(String schemafilename) throws IOException, InterruptedException{
   	 BufferedReader bufferedReader = new BufferedReader(new FileReader(schemafilename));
	 String line;
	 while ((line = bufferedReader.readLine()) != null){
			 ColumnMetadata columnMetadata= new ColumnMetadata();
			 String[] sections=line.split(":");
			 columnMetadata.name=sections[0];
			 String[] terms=sections[1].split(",");
			 columnMetadata.isIndexed=true;			 
			 columnMetadata.dataType=terms[0];
			 if("indexed".equals(terms[1])){
				 columnMetadata.isIndexed=true;
			 }
			 else{
				 columnMetadata.isIndexed=false;
			 }		 
			 
		     if(terms[2].equals("T")){
		    	 databaseMetaData.xAxisColumns.put(columnMetadata.name,columnMetadata);	    	   
		     }
		     if(terms[3].equals("T")){
		    	 databaseMetaData.yAxisColumns.put(columnMetadata.name,columnMetadata);	    	   
		     }
		       
		     if(terms[4].equals("T")){
		    	 databaseMetaData.zAxisColumns.put(columnMetadata.name,columnMetadata);	    	   
		     }
		       
		     if(terms[5].equals("T")){
		    	 databaseMetaData.predicateColumns.put(columnMetadata.name,columnMetadata);	    	   
		     }
		     if (terms[6].equals("T")) {
		    	 columnMetadata.unit = terms[7];
		     }
		    Column column = new Column(columnMetadata, this);
			 
		 }
		 
		bufferedReader.close();		
	}
	
	

    private void loadData(String datafilename) throws IOException{
      	BufferedReader bufferedReader = new BufferedReader(new FileReader(datafilename));
		String line;
		line = bufferedReader.readLine();
		String[] header=line.split(",");
		int count=0;
		 String[] terms;
		while ((line = bufferedReader.readLine()) != null){
			 terms=line.split(",");
            for(int i=0;i<header.length;i++){
           	     addValue(header[i], count, terms[i]);
                }
            count=count+1;	 		
		 }
		this.rowCount=count;
		 
		bufferedReader.close();
	}
    
    
    public Object getColumns(FilterPredicate filterPredicate){
		Column column=columns.get(filterPredicate.getPropertyName());
		return column.getValues(filterPredicate);
	 }
 
    
    public Object getColumns(String columnName){
		Column column = columns.get(columnName);
		return column.getValues();
	 }
 
   
    public ColumnMetadata getColumnMetaData(String columnName){
		Column column = columns.get(columnName);
		return column.columnMetadata;
			
     }
 
     public DatabaseMetaData getFormMetdaData(){
	  return databaseMetaData;
      }


}
