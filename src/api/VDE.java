package api;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import normalization.Normalization;
import normalization.Original;
import normalization.Zscore;

import org.vde.database.DataLoader;
import org.vde.database.Database;
import org.vde.database.Executor;

import piecewiseaggregation.PiecewiseAggregation;
import visual.Result;
import cluster.Clustering;
import cluster.DBScan;
import cluster.KMeans;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import distance.DTWDistance;
import distance.Distance;
import distance.Euclidean;
import analysis.Analysis;
import analysis.Outlier;
import analysis.Representative;
import analysis.Similarity;
import analysis.utility.ChartOutput;
import analysis.utility.DataReformation;

public class VDE {
//	private InMemoryDatabase inMemoryDatabase;
	private Map<String,Database> inMemoryDatabases = new HashMap<String,Database>();

	private Database inMemoryDatabase;
	
	public Executor executor = new Executor(inMemoryDatabase);
	public Analysis analysis;
	public Distance distance;
	public Normalization normalization;
	public Normalization outputNormalization;
	public PiecewiseAggregation paa;
	public ArrayList<List<Double>> data;

	public VDE(){}
	
	public void loadOutlierData() throws IOException, InterruptedException{
		inMemoryDatabase = DataLoader.createDatabase("student");
		inMemoryDatabases.put("student", inMemoryDatabase);
		inMemoryDatabase = DataLoader.createDatabase("financial","src/Financial_Sample.txt","src/Financial_Sample.csv");
		inMemoryDatabases.put("financial",inMemoryDatabase);
		inMemoryDatabase = DataLoader.createDatabase("income","src/census_test_schema.txt","src/census-income-test.csv");
		inMemoryDatabases.put("income", inMemoryDatabase);
	}
	
	public String getData(String query) throws InterruptedException, IOException{
		// get data from database
		 Args args = new ObjectMapper().readValue(query,Args.class);
		 Query  q = new Query("query").setGrouby(args.groupBy+","+args.xAxis).setAggregationFunc(args.aggrFunc).setAggregationVaribale(args.aggrVar);
		 LinkedHashMap<String, LinkedHashMap<Integer, Float>> output = executor.changeFromNodeToLinkedList(executor.getData(q));
		 // setup result format
		 Result finalOutput = new Result();
		 finalOutput.method = args.method;
		 finalOutput.xUnit = inMemoryDatabase.getColumnMetaData(args.xAxis).unit;
		 finalOutput.yUnit = inMemoryDatabase.getColumnMetaData(args.yAxis).unit;
		 // generate new result for query
		 ChartOutput chartOutput = new ChartOutput(finalOutput, args);
		 // generate the corresponding distance metric
		 if (args.distance_metric.equals("Euclidean")) {
			 distance = new Euclidean();
		 }
		 else {
			 distance = new DTWDistance();
		 }
		 // generate the corresponding data normalization metric
		 if (args.distanceNormalized) {
			 normalization = new Zscore();
		 }
		 else {
			 normalization = new Original();
		 }
		 // generate the corresponding output normalization
		 if (args.outputNormalized) {
			 outputNormalization = new Zscore();
		 }
		 else {
			 outputNormalization = new Original();
		 }
		 // reformat database data
		 DataReformation dataReformatter = new DataReformation(outputNormalization);
		 double[][] normalizedgroups = dataReformatter.reformatData(output);
		 // generate the corresponding analysis method
		 if (args.method.equals("Outlier")) {
			 Clustering cluster = new DBScan(distance, normalization, args);
			 analysis = new Outlier(executor,inMemoryDatabase,chartOutput,distance,normalization,cluster);
		 }
		 else if (args.method.equals("RepresentativeTrends")) {
			 Clustering cluster = new KMeans(distance, normalization, args);
			 analysis = new Representative(executor,inMemoryDatabase,chartOutput,distance,normalization, cluster);
		 }
		 else if (args.method.equals("SimilaritySearch")) {
			 paa = new PiecewiseAggregation(normalization, args, inMemoryDatabase);
			 analysis = new Similarity(executor,inMemoryDatabase,chartOutput,distance,normalization,paa);
			 ((Similarity) analysis).setDescending(false);
		 }
		 else if (args.method.equals("DissimilaritySearch")) {
			 paa = new PiecewiseAggregation(normalization, args, inMemoryDatabase);
			 analysis = new Similarity(executor,inMemoryDatabase,chartOutput,distance,normalization,paa);
			 ((Similarity) analysis).setDescending(true);
		 }
		 analysis.generateAnalysis(output, normalizedgroups);
		 ObjectMapper mapper = new ObjectMapper();
		 return mapper.writeValueAsString(analysis.getChartOutput().finalOutput);
	}

	
	
	public String outlier(String method,String sql,String outliercount) throws IOException{
		return readFile();
	}
	
	
	public String getDatabaseNames() throws JsonProcessingException{
		return new ObjectMapper().writeValueAsString(inMemoryDatabases.keySet());
	}
	
	
	public String getInterfaceFomData(String query) throws IOException{
		FormQuery fq = new ObjectMapper().readValue(query,FormQuery.class);
		inMemoryDatabase = inMemoryDatabases.get(fq.getDatabasename());
		executor = new Executor(inMemoryDatabase);
		return new ObjectMapper().writeValueAsString(inMemoryDatabases.get(fq.getDatabasename()).getFormMetdaData());
	}
	
	public String readFile() throws IOException {
	    BufferedReader br = new BufferedReader(new FileReader("/src/data1.txt"));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();
	        while (line != null) {
	            sb.append(line);
	            line = br.readLine();
	        }
	        return sb.toString();
	    } finally {
	        br.close();
	    }
	}
	
}