/**
 * 
 */
package piecewiseaggregation;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.vde.database.ColumnMetadata;
import org.vde.database.Database;

import api.Args;
import api.Point;
import normalization.Normalization;

/**
 * @author xiaofo
 *
 */
public class PiecewiseAggregation {
	public Normalization normalization;
	public Args args;
	public Database inMemoryDatabase;
	
	/**
	 * @param normalization
	 * @param args
	 * @param inMemoryDatabase
	 */
	public PiecewiseAggregation(Normalization normalization, Args args,
			Database inMemoryDatabase) {
		this.normalization = normalization;
		this.args = args;
		this.inMemoryDatabase = inMemoryDatabase;
	}

	public double[] applyPAA(Set<Integer> ignore) {
		return null;
	}
	
	public double[][] applyPAAonData(LinkedHashMap<String,LinkedHashMap<Integer,Float>> data, Set<Integer> ignore){
		double[][] normalizedgroups = new double[data.size()][];
		int count = 0;
		ColumnMetadata columnMetadata = inMemoryDatabase.getColumnMetaData(args.xAxis);
		float pAAWidth = columnMetadata.pAAWidth;
		int numberofsegments = (int) (1/pAAWidth);
		float min = columnMetadata.min;
		float max = columnMetadata.max;	
		float range = max-min;
	
	  
		for (String key : data.keySet()) {
			Map<Integer,Float> values = data.get(key);
			double [] normalizedValues = new double[numberofsegments+1];
			int[] numberofpoints = new int[numberofsegments+1];
			for (int i = 0; i < numberofsegments; i++) {
				normalizedValues[i] = 0.0;
				numberofpoints[i] = 0;
				ignore.add(i);
			}
		  
			for (Integer key1 : values.keySet()) {
				if (range == 0)
					continue;
				int segment = (int) (((key1-min))/(range*pAAWidth));
				normalizedValues[segment] = normalizedValues[segment] + values.get(key1);
				numberofpoints[segment] = numberofpoints[segment]+1;
				ignore.remove(key1);
			}
		  
			for (int i = 0; i <= numberofsegments; i++) {
				if (numberofpoints[i] > 0)
					normalizedValues[i] = normalizedValues[i] / numberofpoints[i];
			}
		  
			for (int i = 0; i < numberofsegments; i++) {
				if (numberofpoints[i] == 0) {
					if (i > 0 && i < numberofsegments) {
						normalizedValues[i] = normalizedValues[i-1] + normalizedValues[i+1]/2;
					}
					else if(i > 0) {
						normalizedValues[i] = normalizedValues[i-1];
					}
					else{
						normalizedValues[i] = normalizedValues[i+1];
					}
				}
				normalization.normalize(normalizedValues);
			}
			  
			  
			normalizedgroups[count] = normalizedValues;
			count++;		  
		}
		return normalizedgroups;  
	}
	
	public double[] applyPAAonQuery(Set<Integer> ignore){
		ColumnMetadata xcolumnMetadata = inMemoryDatabase.getColumnMetaData(args.xAxis);
		float pAAWidth = xcolumnMetadata.pAAWidth;
		int numberofsegments = (int) (1/pAAWidth);
		float min = args.getSketchPoints().minX;
		float max = args.getSketchPoints().maxX;		
		float rangeX = max-min;
		double [] normalizedValues = new double[numberofsegments+1];
		int[] numberofpoints = new int[numberofsegments+1];
		for (int i = 0;i < numberofsegments;i++) {
			normalizedValues[i] = 0.0;
			numberofpoints[i] = 0;
		}
	  
		ColumnMetadata ycolumnMetadata = inMemoryDatabase.getColumnMetaData(args.yAxis);
		float minY = ycolumnMetadata.min;
		float maxY = ycolumnMetadata.max;
		float rangeY = maxY-minY;
	  
		float minYQ = args.getSketchPoints().minY;
		float maxYQ = args.getSketchPoints().maxY;		
		float rangeYQ = maxYQ-minYQ;
		for (Point p : args.sketchPoints.points) {		  
			int segment = (int) ((p.getX()-min)/(rangeX*pAAWidth));
			float yvalue = minY+((p.getY()-minYQ)*rangeY/(rangeYQ));
			normalizedValues[segment] = normalizedValues[segment]+yvalue;
			numberofpoints[segment] = numberofpoints[segment]+1;
		}
		  
		for (int i = 0; i <= numberofsegments; i++) {
			if(numberofpoints[i] > 0)
				normalizedValues[i] = normalizedValues[i]/numberofpoints[i];
		}
		  
		for (int i = 0; i < numberofsegments; i++) {
			if (numberofpoints[i] == 0) {
				if (i > 0 && i < numberofsegments) {
					normalizedValues[i] = normalizedValues[i-1] + normalizedValues[i+1]/2;
				}
				else if (i > 0) {
					normalizedValues[i] = normalizedValues[i-1];
				}
				else {
					normalizedValues[i] = normalizedValues[i+1];
				}
			}
			
			if (ignore.contains(i))
				normalizedValues[i] = 0;
				
		}
		normalization.normalize(normalizedValues); 
		  
		return normalizedValues;
	}
}
