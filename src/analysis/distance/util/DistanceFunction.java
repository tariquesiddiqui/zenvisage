package analysis.distance.util;


public interface DistanceFunction
{
   public double calcDistance(double[] vector1, double[] vector2);
}