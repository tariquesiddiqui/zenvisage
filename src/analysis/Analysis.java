/**
 * 
 */
package analysis;

import java.util.LinkedHashMap;

import normalization.Normalization;

import org.vde.database.Database;
import org.vde.database.Executor;

import com.fasterxml.jackson.core.JsonProcessingException;

import distance.Distance;
import analysis.utility.ChartOutput;

/**
 * @author xiaofo
 * Super class of all data analysis subclasses.
 */
public abstract class Analysis {
	/** 
	 * Variables of analysis class
	 */
	public Executor executor;
	public Database inMemoryDatabase;
	public ChartOutput chartOutput;
	public Distance distance;
	public Normalization normalization;

	/**
	 * @param executor
	 * @param inMemoryDatabase
	 * @param chartOutput
	 * @param distance
	 * @param normalization
	 */
	public Analysis(Executor executor, Database inMemoryDatabase,
			ChartOutput chartOutput, Distance distance,
			Normalization normalization) {
		this.executor = executor;
		this.inMemoryDatabase = inMemoryDatabase;
		this.chartOutput = chartOutput;
		this.distance = distance;
		this.normalization = normalization;
	}

	/**
	 * General method for getting analysis data.
	 * @param output TODO
	 * @param normalizedgroups TODO
	 * @throws JsonProcessingException 
	 */
	public abstract void generateAnalysis(LinkedHashMap<String, LinkedHashMap<Integer, Float>> output, double[][] normalizedgroups) throws JsonProcessingException;

	/**
	 * @return the executor
	 */
	public Executor getExecutor() {
		return executor;
	}

	/**
	 * @param executor the executor to set
	 */
	public void setExecutor(Executor executor) {
		this.executor = executor;
	}

	/**
	 * @return the inMemoryDatabase
	 */
	public Database getInMemoryDatabase() {
		return inMemoryDatabase;
	}

	/**
	 * @param inMemoryDatabase the inMemoryDatabase to set
	 */
	public void setInMemoryDatabase(Database inMemoryDatabase) {
		this.inMemoryDatabase = inMemoryDatabase;
	}

	/**
	 * @return the chartOutput
	 */
	public ChartOutput getChartOutput() {
		return chartOutput;
	}

	/**
	 * @param chartOutput the chartOutput to set
	 */
	public void setChartOutput(ChartOutput chartOutput) {
		this.chartOutput = chartOutput;
	}

	/**
	 * @return the distance
	 */
	public Distance getDistance() {
		return distance;
	}

	/**
	 * @param distance the distance to set
	 */
	public void setDistance(Distance distance) {
		this.distance = distance;
	}

	/**
	 * @return the normalization
	 */
	public Normalization getNormalization() {
		return normalization;
	}

	/**
	 * @param normalization the normalization to set
	 */
	public void setNormalization(Normalization normalization) {
		this.normalization = normalization;
	}
}
