/**
 * 
 */
package analysis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import normalization.Normalization;

import org.apache.commons.collections.map.MultiValueMap;
import org.vde.database.Database;
import org.vde.database.Executor;

import com.fasterxml.jackson.core.JsonProcessingException;

import cluster.Clustering;
import distance.Distance;
import analysis.utility.ChartOutput;

/**
 * @author xiaofo
 *
 */
public class Outlier extends Analysis {
	/**
	 * Cluster class for clustering.
	 */
	public Clustering cluster;
	
	public Outlier(Executor executor, Database inMemoryDatabase,
			ChartOutput chartOutput, Distance distance, Normalization normalization, Clustering cluster) {
		super(executor, inMemoryDatabase, chartOutput, distance, normalization);
		// TODO Auto-generated constructor stub
		this.cluster = cluster;
	}

	/* (non-Javadoc)
	 * @see analyze.Analysis#getAnalysis()
	 */
	@Override
	public void generateAnalysis(LinkedHashMap<String, LinkedHashMap<Integer, Float>> output, double[][] normalizedgroups) throws JsonProcessingException {
		// TODO Auto-generated method stub
		ArrayList<String> mappings = new ArrayList<String>();
		for(String key : output.keySet()) {
			 mappings.add(key);
		}
		double eps = cluster.calculateEpsDistance(normalizedgroups, 2);
		@SuppressWarnings("rawtypes")
		List clusters = cluster.calculateClusters(eps*2, 2, normalizedgroups);
		double[][] representativeTrends = cluster.computeRepresentativeTrends(clusters);
		List<Integer> orders = computeOrders(normalizedgroups,representativeTrends);
		chartOutput.chartOutput(normalizedgroups,output,orders,mappings,chartOutput.args,chartOutput.finalOutput);
	}
	
	/**
	 * @param normalizedgroups
	 * @param represetativeTrends
	 * @return orders
	 */
	public List<Integer> computeOrders(double [][] normalizedgroups,double[][] represetativeTrends) {
		List<Integer> orders = new ArrayList<Integer>();
		MultiValueMap indexOrder = new MultiValueMap();
	
	    List<Double> distances = new ArrayList<Double>(); 
		for(int i = 0; i < normalizedgroups.length; i++){
			double min = 1000000;
			for(int j = 0; j < represetativeTrends.length; j++){
				double dist = distance.calculateDistance(normalizedgroups[i], represetativeTrends[j]);
				if(dist < min){
					min = dist;
				}
			}
			distances.add(min);	
			indexOrder.put(min,i);
		}   
		  
		Collections.sort(distances);
		Collections.reverse(distances);
		for(Double d : distances){
			@SuppressWarnings("rawtypes")
			ArrayList values = (ArrayList)indexOrder.get(d);
			Integer val = (Integer) values.get(0);
			orders.add((val));
			indexOrder.remove(d,val);
				 
		}
		return orders;		
	}

}
