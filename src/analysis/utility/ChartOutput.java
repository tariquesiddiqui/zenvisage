package analysis.utility;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import visual.Chart;
import visual.Result;
import api.Args;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * @author xiaofo
 *
 */
public class ChartOutput {
	public Result finalOutput;
	public Args args;
	
	/**
	 * @param finalOutput
	 * @param args
	 */
	public ChartOutput(Result finalOutput, Args args) {
		this.finalOutput = finalOutput;
		this.args = args;
	}
		
	/**
	 * @param result
	 * @param orig
	 * @param orders
	 * @param mappings
	 * @param args
	 * @param finalOutput
	 * @throws JsonProcessingException
	 */
	public void chartOutput(double[][] result,LinkedHashMap<String,LinkedHashMap<Integer,Float>> orig,List<Integer> orders,ArrayList<String> mappings, Args args, Result finalOutput) throws JsonProcessingException{
		if (args.outlierCount==0)
			args.setOutlierCount(4);
			
		for(int i = 0; i < args.outlierCount; i++) {
			// initialize a new chart
			Chart chartOutput = new Chart();
			chartOutput.setxType(finalOutput.method+" "+(i+1)+" : "+mappings.get(orders.get(i)));
			chartOutput.setyType(args.aggrFunc+"("+args.yAxis+")");
			// fill in chart data
			String key = mappings.get(orders.get(i));
			LinkedHashMap<Integer,Float> points = orig.get(key);
			int c = 0;
			for(Integer k : points.keySet()) {
				chartOutput.xData.add(Integer.toString(k));
				chartOutput.yData.add(Double.toString(result[orders.get(i)][c]));
				c++;
			}
			finalOutput.outputCharts.add(chartOutput);
		}

		return;	
	}
}
