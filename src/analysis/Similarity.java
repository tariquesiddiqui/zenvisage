/**
 * 
 */
package analysis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import normalization.Normalization;

import org.apache.commons.collections.map.MultiValueMap;
import org.vde.database.Database;
import org.vde.database.Executor;

import com.fasterxml.jackson.core.JsonProcessingException;

import piecewiseaggregation.PiecewiseAggregation;
import distance.Distance;
import analysis.utility.ChartOutput;

/**
 * @author xiaofo
 *
 */
public class Similarity extends Analysis {
	/* Whether rank trends in descending order */
	public boolean descending = true;
	public PiecewiseAggregation paa;

	public Similarity(Executor executor, Database inMemoryDatabase,
			ChartOutput chartOutput, Distance distance, Normalization normalization, PiecewiseAggregation paa) {
		super(executor, inMemoryDatabase, chartOutput, distance, normalization);
		// TODO Auto-generated constructor stub
		this.paa = paa;
	}

	/* (non-Javadoc)
	 * @see analyze.Analysis#getAnalysis()
	 */
	@Override
	public void generateAnalysis(LinkedHashMap<String, LinkedHashMap<Integer, Float>> output, double[][] normalizedgroups) throws JsonProcessingException {
		// TODO Auto-generated method stub
		ArrayList<String> mappings = new ArrayList<String>();
		for(String key : output.keySet()) {
			 mappings.add(key);
		}
		Set<Integer> ignore = new HashSet<Integer>();
		double[][] normalizedgroup = paa.applyPAAonData(output,ignore);
		double[] queryTrend = paa.applyPAAonQuery(ignore);
		List<Integer> orders = computeOrders(normalizedgroup,queryTrend,mappings);
		chartOutput.chartOutput(normalizedgroups, output, orders, mappings, chartOutput.args, chartOutput.finalOutput);
		return;
	}
	
	/**
	 * @param normalizedgroups
	 * @param queryTrend
	 * @param mappings
	 * @return
	 */
	public List<Integer> computeOrders(double[][] normalizedgroups, double[] queryTrend, ArrayList<String> mappings) {
		List<Integer> orders = new ArrayList<Integer>();
		MultiValueMap indexOrder =new MultiValueMap();
    	List<Double> distances = new ArrayList<Double>(); 
    	for(int i = 0;i < normalizedgroups.length;i++) {
    		double dist = distance.calculateDistance(normalizedgroups[i], queryTrend);
    		distances.add(dist);	
    		indexOrder.put(dist,i);
    	}   
	  
    	Collections.sort(distances);
    	if (descending)
    		Collections.reverse(distances);
    	for(Double d : distances){
    		@SuppressWarnings("rawtypes")
			ArrayList values = (ArrayList)indexOrder.get(d);
		    Integer val = (Integer) values.get(0);
			orders.add((val));
			indexOrder.remove(d,val);
			 
		 }
    	return orders;		
	}

	/**
	 * @return the descending
	 */
	public boolean isDescending() {
		return descending;
	}

	/**
	 * @param descending the descending to set
	 */
	public void setDescending(boolean descending) {
		this.descending = descending;
	}

}
