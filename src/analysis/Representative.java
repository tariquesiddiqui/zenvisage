/**
 * 
 */
package analysis;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import normalization.Normalization;

import org.vde.database.Database;
import org.vde.database.Executor;

import com.fasterxml.jackson.core.JsonProcessingException;

import cluster.Clustering;
import distance.Distance;
import analysis.utility.ChartOutput;

/**
 * @author xiaofo
 *
 */
public class Representative extends Analysis {
	/**
	 * Cluster class for clustering.
	 */
	public Clustering cluster;

	/**
	 * @param executor
	 * @param inMemoryDatabase
	 * @param chartOutput
	 * @param distance
	 * @param normalization
	 * @param cluster
	 */
	public Representative(Executor executor, Database inMemoryDatabase,
			ChartOutput chartOutput, Distance distance, Normalization normalization, Clustering cluster) {
		super(executor, inMemoryDatabase, chartOutput, distance, normalization);
		// TODO Auto-generated constructor stub
		this.cluster = cluster;
	}

	/* (non-Javadoc)
	 * @see analyze.Analysis#getAnalysis()
	 */
	@Override
	public void generateAnalysis(LinkedHashMap<String, LinkedHashMap<Integer, Float>> output, double[][] normalizedgroups) throws JsonProcessingException {
		// TODO Auto-generated method stub
		ArrayList<String> mappings = new ArrayList<String>();
		for(String key : output.keySet()) {
			 mappings.add(key);
		}
		double eps = cluster.calculateEpsDistance(normalizedgroups, 2);
		@SuppressWarnings({ "rawtypes"})
		List clusters = cluster.calculateClusters(eps, 0, normalizedgroups);
		double[][] representativeTrends = cluster.computeRepresentativeTrends(clusters);
		List<Integer> orders = new ArrayList<Integer>();
		for(int i = 0; i < representativeTrends.length; i++) {
			orders.add(i);
		}
		chartOutput.chartOutput(normalizedgroups,output,orders,mappings,chartOutput.args,chartOutput.finalOutput);
	}

}
