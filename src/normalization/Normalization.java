/**
 * 
 */
package normalization;

/**
 * @author xiaofo
 * @category Normalization interface for data normalization
 */
public interface Normalization {
	
	/**
	 * Perform normalization in place of given array.
	 * @param normalizedValues
	 */
	public void normalize(double[] normalizedValues);
}
