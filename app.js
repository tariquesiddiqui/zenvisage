  var java = require("java");
  var io = require("socket.io");
  var express = require("express");
  var hbs = require("express-hbs");
  var connectAssets = require("connect-assets");
  var fs = require('fs');
  var connect = require('connect');
  var querystring = require('querystring');
  var logger = require('morgan');
  var url= require('url');

  var commandLineArgs = require('minimist')(process.argv.slice(2));

  if (commandLineArgs._[0] === "help") {
    console.info("help: display help");
    console.info("--port n: run on port n");
    console.info("--debug n: run java in debug mode, connect with Eclipse on port n");
    process.exit(0);
  }

  java.classpath.push("lib/postgresql-9.2-1000.jdbc4.jar");
  java.classpath.push("lib/py4j0.8.jar");
  java.classpath.push("lib/jackson-annotations-2.3.0.jar");
  java.classpath.push("lib/jackson-core-2.3.0.jar");
  java.classpath.push("lib/jackson-databind-2.3.0.jar");
  java.classpath.push("lib/guava-15.0.jar");
  java.classpath.push("lib/commons-math3-3.4.1.jar");
  java.classpath.push("lib/javaml-0.1.5.jar");
  java.classpath.push("lib/commons-collections-3.2.1.jar");
  java.classpath.push("bin/");

  java.options.push("-Xmx6g");

  // read debug command line option, and if it's present enable debug mode
  if (commandLineArgs.debug) {
    var debugPort = commandLineArgs.debug;

    java.options.push("-Xdebug");
    java.options.push("-agentlib:jdwp=transport=dt_socket,address=" +
      debugPort + ",server=y,suspend=n");
  }

  var vde = java.newInstanceSync("api.VDE");
  //  console.log(vde.sumSync(1,1));

  vde.loadOutlierDataSync();

  var app = connect()
  .use(logger())
  .use(connect.static('new-web')) //
  .use(function(request, response) {
    request.socket.setTimeout(1000000); // ms
    var urlObj = url.parse(request.url);
    var pathname = urlObj.pathname;
    //console.log(pathname);

    function sendResponse(body) {
      response.writeHead(200, {
        'Content-Type': 'text/json',
        'Content-Length': body.length,
      });
      response.write(body);
      response.end();
    }

    if (pathname == '/getdata') {
      var query = decodeURI(urlObj.query);
      var output = vde.getDataSync(query);
      //console.log(output);
      sendResponse(output);
     }
    
    if (pathname == '/getformdata') {
    	//console.log(urlObj);
        //console.log(urlObj.query);
        var query = decodeURI(urlObj.query);
        var output = vde.getInterfaceFomDataSync(query);
        console.log(output);
        sendResponse(output);
       }
  })
  .listen(8999);
